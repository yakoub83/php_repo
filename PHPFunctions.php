<?php
	
	function doDate($inNational,$inInterNational)
	{
	
      $date1=date_create($inNational);
	  $date2=date_create($inInterNational);
      echo date_format($date1,"m/d/Y ");//mm/dd/yyyy format.
	  echo ", ";
	  echo date_format($date2,"d/m/Y "); // dd/mm/yyyy international format
	}

function doString($instring )
	{
		
		echo "This String has ".strlen($instring)." characters"; // Get The Length of a String
		echo " , ";
	    echo trim($instring," "); //Trim any leading or trailing whitespace
		echo " , ";	
	    echo strtolower($instring);  //Display the string as all lowercase characters
		echo " , ";	
		echo strripos($instring, "DMACC");// whether or not the string contains "DMACC" either upper or lowercase
		echo " , ";
		
		// another way with the use of the if statment to know whether or not the string contains "DMACC"
		if (strripos($instring, "DMACC") !== false) {
         echo 'The string contains "DMACC"';
          }
  else {
			 echo 'The string DOES NOT contains "DMACC"';
       }
		
	}
	
function doNumbers($inNumber1,$inNumber2)
{
	echo "formatted number: ".number_format($inNumber1);  //format numbers using number_format()
	echo " --- ";
	echo "US currency: ";
	setlocale(LC_MONETARY, 'en_US');
    echo money_format('%(#10n',$inNumber2);    //format currency using money_format()
	
}
?> 
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
</head>

<body>

<p> formated Dates: <?php  echo doDate("2018-02-15","2018-03-15"); ?></p>
<p> <?php echo doString(" dmacc Intro To PHP ");?> </p>
<p>  <?php echo doNumbers(1234567890,123456);?> </p>

</body>
</html>

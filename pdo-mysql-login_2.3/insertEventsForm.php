<?php
	require 'connEventUser.php';
	if(empty($_SESSION['validUser']))
		header('Location: login.php');


	//Setup the variables used by the page
		//field data
	$event_id="";	
	$event_name="";
	$eventDescription="";
	$eventPresenter="";
	$eventDate="";
	$eventTime="";

		//error messages

	$event_nameErrMsg="";
	$eventDescriptionErrMsg="";
	$eventPresenterErrMsg="";
	$eventDateErrMsg="";
	$eventTimeErrMsg="";	
	


		$validForm = false;

		$todaysDate = date("Y-m-d");		//use today's date as the default input to the date( )
		
				
	if(isset($_POST["submit"]))
	{	
		
    //$id = $_POST['event_id'];
    $event_name=$_POST['event_name'];
	$eventDescription=$_POST['event_description'];
	$eventPresenter=$_POST['event_presenter'];
    $eventDate=$_POST['event_date'];   
    $eventTime=$_POST['event_time'];   
	
		//VALIDATION FUNCTIONS		Use functions to contain the code for the field validations.  
			function validateName($inVal){
		
		global $validForm, $event_nameErrMsg;
			$event_nameErrMsg = "";
			
        if($inVal=="") {
			$validForm = false;
            $event_nameErrMsg= "this field can not be empty";
        }
		
	}// end validateName()
        
	function validateDescription($inVal){
		
		global $validForm, $eventDescriptionErrMsg;
			$eventDescriptionErrMsg = "";
		
        if($inVal=="") 
		{
			 $validForm = false;
            $eventDescriptionErrMsg="this field can not be empty";
        }
		
	}//end  validateDescription()
	
	 function validatePresenter ($inVal){
		 
		 global $validForm, $eventPresenterErrMsg;
			$eventPresenterErrMsg = "";
        
        if($inVal=="") {
			
            $eventPresenterErrMsg= "this field can not be empty";
        } 
	 }// end validatePresenter()
	
		
	function validateDate($inVal){
		
		 global $validForm, $eventDateErrMsg;
			$eventDateErrMsg = "";
		
		if($inVal=="") {
			
            $eventDateErrMsg= "this field can not be empty";
        }  
		
	}//end   validateDate()
		
		
		function validateTime($inVal){
		
		 global $validForm, $eventTimeErrMsg;
			$eventTimeErrMsg = "";
		
		if($inVal=="") {
			
            $eventTimeErrMsg= "this field can not be empty";
        }  
		
	}//end   validateTime()
			
		
		//VALIDATE FORM DATA  using functions defined above
		$validForm = true;		//switch for keeping track of any form validation errors
		
		validateName($event_name);
		validateDescription($eventDescription);
		validatePresenter($eventPresenter);
		validateDate($eventDate);
		validateTime($eventTime);
		
		if($validForm)
		{
			$message = "All good";	
			
			try {
				
				require 'connection.php';	//CONNECT to the database
				
				// prepare sql and bind parameters
				$stmt = $conn->prepare("INSERT INTO wdv341_event (event_name, event_description, event_presenter, event_date, event_time)
				VALUES (:eventname, :eventdescription, :eventpresenter, :eventdate, :eventtime )");
				$stmt->bindParam(':eventname', $event_name);
				$stmt->bindParam(':eventdescription', $eventDescription);
				$stmt->bindParam(':eventpresenter', $eventPresenter);
				$stmt->bindParam(':eventdate', $eventDate);
				$stmt->bindParam(':eventtime', $eventTime);
				
				//EXECUTE the prepared statement
				$stmt->execute();	
				
				$message = "The Event has been added.";
				
				
				
			}
			
			catch(PDOException $e)
			{
				$message = "There has been a problem. The system administrator has been contacted. Please try again later.";
	
				error_log($e->getMessage());			//Delivers a developer defined error message to the PHP log file at c:\xampp/php\logs\php_error_log
				error_log(var_dump(debug_backtrace()));
			
				//Clean up any variables or connections that have been left hanging by this error.		
			
				header('Location: files/505_error_response_page.php');	//sends control to a User friendly page					
			}

		}
		else
		{
			$message = "Something went wrong";
		}//ends check for valid form		

	}/////////
	
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Presenting Information Technology</title>
	<link rel="stylesheet" href="css/pit.css">  
  	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">  	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  
  	
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
 	  
 	    	<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>  	
  	  

	<script>
		$(function() {
			$('#event_date').datepicker({dateFormat: "yy-mm-dd"});	// match database expected format
		} );
		
		$(function() {
			$('#event_time').timepicker({timeFormat: "HH:mm:ss" });	//match database expected format
		} );
			
		
	</script>   

    <script>
		function clearForm() {
			//alert("inside clearForm()");
			$('.errMsg').html("");					//Clear all span elements that have a class of 'errMsg'. 		
			$('input:text').removeAttr('value');	//REMOVE the value attribute supplied by PHP Validations
			$('textarea').html("");					//Clear the textarea innerHTML
		}
	</script>


</head>

<body>

<div id="container">

	
    <main>
    
		<?php
            //If the form was submitted and valid and properly put into database display the INSERT result message
			if($validForm)
			{
        ?>
      <h1><?php echo $message ?></h1>
    <a href="selectEvents.php">Display Events</a> <br>
    <br><a href="logout.php">Logout of Administrator</a>
        <?php
			}
			else	//display form  errMesg
			{
        ?>
        <form id="updateEventForm" name="updateEventForm" method="post" action="">
        	<fieldset>
              <legend>New Event</legend>
              <p>
                <label for="event_name">Event name: </label>  
                <input type="text" name="event_name" id="event_name" value="<?php echo $event_name;?>" /> 
                <span class="errMsg"> <?php echo $event_nameErrMsg; ?></span>
              </p>
              <p>
                <label for="event_description">Event Description:</label>
                 <input type="text" name="event_description" id="event_description" value="<?php echo $eventDescription;?>" /> 
                <span class="errMsg"><?php echo $eventDescriptionErrMsg; ?></span>                
              </p>
              <p>
                <label for="event_presenter">Presenter: </label>
                <input type="text" name="event_presenter" id="event_presenter" value="<?php echo $eventPresenter;  ?>" /> 
                <span class="errMsg"><?php echo $eventPresenterErrMsg; ?></span>                      
              </p>
              <p>
                <label for="event_date">Date: </label> 
                <input type="text" name="event_date" id="event_date" value="<?php echo $eventDate;  ?>"/>
                <span class="errMsg"><?php echo $eventDateErrMsg; ?></span>      
              </p>
              <p>
                <label for="event_time">Event Time: </label> 
                <input type="text" name="event_time" id="event_time" value="<?php echo $eventTime;  ?>"/>
                <span class="errMsg"><?php echo $eventTimeErrMsg; ?></span>                
              </p>
              
             
             
                       
              
          </fieldset>
         	<p>
            	<input type="submit" name="submit" id="submit" value="Insert Event" />
            	<input type="reset" name="button2" id="button2" value="Clear Form" onClick="clearForm()" />
        	</p>  
      </form>
        <?php
			}//end else
        ?>    	
	</main>
    
	

</div>
</body>
</html>

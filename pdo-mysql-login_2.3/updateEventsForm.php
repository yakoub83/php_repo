<?php
	require 'connEventUser.php';
	if(empty($_SESSION['validUser']))
		header('Location: login.php');

	//Setup the variables used by the page
		//field data
	$event_id="";	
	$event_name="";
	$eventDescription="";
	$eventPresenter="";
	$eventDate="";
	$eventTime="";

		//error messages

	$event_nameErrMsg="";
	$eventDescriptionErrMsg="";
	$eventPresenterErrMsg="";
	$eventDateErrMsg="";
	$eventTimeErrMsg="";	
	


		$validForm = false;

		//mysql DATE stores data in a YYYY-MM-DD format
		$todaysDate = date("Y-m-d");		//use today's date as the default input to the date( )
		
		//The form needs to display the fields of the selected record
		$updateeventID = $_GET['event_id'];	//Record Id to be updated
		//$updateeventID = 2;				//Hard code a key for testing purposes	
				
	if(isset($_POST["submit"]))
	{	
		//The form has been submitted and needs to be processed
		
		
		//Validate the form data here!
	
		//Get the name value pairs from the $_POST variable into PHP variables
		//This example uses PHP variables with the same name as the name atribute from the HTML form
		
    //$id = $_POST['event_id'];
    $event_name=$_POST['event_name'];
	$eventDescription=$_POST['event_description'];
	$eventPresenter=$_POST['event_presenter'];
    $eventDate=$_POST['event_date'];   
    $eventTime=$_POST['event_time'];   
	
		//VALIDATION FUNCTIONS		Use functions to contain the code for the field validations.  
			function validateName($inVal){
		
		global $validForm, $event_nameErrMsg;
			$event_nameErrMsg = "";
			
        if($inVal=="") {
			$validForm = false;
            $event_nameErrMsg= "this field can not be empty";
        }
		
	}// end validateName()
        
	function validateDescription($inVal){
		
		global $validForm, $eventDescriptionErrMsg;
			$eventDescriptionErrMsg = "";
		
        if($inVal=="") 
		{
			 $validForm = false;
            $eventDescriptionErrMsg="this field can not be empty";
        }
		
	}//end  validateDescription()
	
	 function validatePresenter ($inVal){
		 
		 global $validForm, $eventPresenterErrMsg;
			$eventPresenterErrMsg = "";
        
        if($inVal=="") {
			
            $eventPresenterErrMsg= "this field can not be empty";
        } 
	 }// end validatePresenter()
	
		
	function validateDate($inVal){
		
		 global $validForm, $eventDateErrMsg;
			$eventDateErrMsg = "";
		
		if($inVal=="") {
			
            $eventDateErrMsg= "this field can not be empty";
        }  
		
	}//end   validateDate()
		
		
		function validateTime($inVal){
		
		 global $validForm, $eventTimeErrMsg;
			$eventTimeErrMsg = "";
		
		if($inVal=="") {
			
            $eventTimeErrMsg= "this field can not be empty";
        }  
		
	}//end   validateTime()
			
		
		//VALIDATE FORM DATA  using functions defined above
		$validForm = true;		//switch for keeping track of any form validation errors
		
		validateName($event_name);
		validateDescription($eventDescription);
		validatePresenter($eventPresenter);
		validateDate($eventDate);
		validateTime($eventTime);
		
		if($validForm)
		{
			$message = "All good";	
			
			try {
				
				require 'connection.php';	//CONNECT to the database
				
				//Create the SQL command string
				$sql = "UPDATE wdv341_event SET ";
				$sql .= "event_name='$event_name', ";
				$sql .= "event_description='$eventDescription', ";
				$sql .= "event_presenter='$eventPresenter', ";
				$sql .= "event_date='$eventDate', ";
				$sql .= "event_time='$eventTime' ";
			                                               //Last column does NOT have a comma after it.
				$sql .= "WHERE event_id='$updateeventID'";
				
				//PREPARE the SQL statement
				$stmt = $conn->prepare($sql);
				
				//BIND the values to the input parameters of the prepared statement
				
			
				$stmt->bindParam(':eventname', $event_name);
				$stmt->bindParam(':eventdescription', $eventDescription);
				$stmt->bindParam(':eventpresenter', $eventPresenter);
				$stmt->bindParam(':eventdate', $eventDate);
				$stmt->bindParam(':eventtime', $eventTime);
				
				
				//EXECUTE the prepared statement
				$stmt->execute();	
				
				$message = "The Event has been Updated.";
				
			}
			
			catch(PDOException $e)
			{
				$message = "There has been a problem. The system administrator has been contacted. Please try again later.";
	
				error_log($e->getMessage());			//Delivers a developer defined error message to the PHP log file at c:\xampp/php\logs\php_error_log
				error_log(var_dump(debug_backtrace()));
			
				//Clean up any variables or connections that have been left hanging by this error.		
			
				header('Location: files/505_error_response_page.php');	//sends control to a User friendly page					
			}

		}
		else
		{
			$message = "Something went wrong";
		}//ends check for valid form		

	}
	else
	{
		//Form has not been seen by the user.  display the form with the selected event information	
		try {
		  
		  require 'connection.php';	//CONNECT to the database
		  
		  //mysql DATE stores data in a YYYY-MM-DD format
		  $todaysDate = date("Y-m-d");		//use today's date as the default input to the date( )
		  
		  //Create the SQL command string
		  $sql = "SELECT ";
		  $sql .= "event_id, ";
		  $sql .= "event_name, ";
		  $sql .= "event_description, ";
		  $sql .= "event_presenter, ";
		  $sql .= "event_date, ";
		  $sql .= "event_time";//Last column does NOT have a comma after it.
		  $sql .= "FROM wdv341_event ";
		  $sql .= "WHERE event_id=$updateeventID";
		  
		  //PREPARE the SQL statement
		  $stmt = $conn->prepare($sql);
		  
		  //EXECUTE the prepared statement
		  $stmt->execute();		
		  
		  //RESULT object contains an associative array
		  $stmt->setFetchMode(PDO::FETCH_ASSOC);	
		  
		  $row=$stmt->fetch(PDO::FETCH_ASSOC);	 
			
			 $id=$row['event_id'];
			$event_name=$row['event_name'];
			$eventDescription=$row['event_description'];
			$eventPresenter=$row['event_presenter'];
			$eventDate=$row['event_date'];
			$eventTime=$row['event_time'];   
			
			
			
			
	  }
	  
	  catch(PDOException $e)
	  {
		  $message = "There has been a problem. The system administrator has been contacted. Please try again later.";
	
		  error_log($e->getMessage());			//Delivers a developer defined error message to the PHP log file at c:\xampp/php\logs\php_error_log
		  error_log($e->getLine());
		  error_log(var_dump(debug_backtrace()));
	  
		  //Clean up any variables or connections that have been left hanging by this error.		
	  
		 // header('Location: files/505_error_response_page.php');	//sends control to a User friendly page					
	  }	
		
	}// ends if submit 
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Presenting Information Technology</title>
	<link rel="stylesheet" href="css/pit.css">  
  	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">  	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  
  	
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
 	  
 	    	<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>  	
  	  

	<script>
		$(function() {
			$('#event_date').datepicker({dateFormat: "yy-mm-dd"});	// match database expected format
		} );
		
		$(function() {
			$('#event_time').timepicker({timeFormat: "HH:mm:ss" });	//match database expected format
		} );
			
		
	</script>   

    <script>
		function clearForm() {
			//alert("inside clearForm()");
			$('.errMsg').html("");					//Clear all span elements that have a class of 'errMsg'. 		
			$('input:text').removeAttr('value');	//REMOVE the value attribute supplied by PHP Validations
			$('textarea').html("");					//Clear the textarea innerHTML
		}
	</script>


</head>

<body>

<div id="container">

	
    <main>
    
		<?php
            //If the form was submitted and valid and properly put into database display the INSERT result  message
			if($validForm)
			{
        ?>
      <h1><?php echo $message ?></h1>
    <a href="selectEvents.php">Display Events</a> <br>
    <br><a href="logout.php">Logout of Administrator</a>
        <?php
			}
			else	//display form  errMesg
			{
        ?>
        <form id="updateEventForm" name="updateEventForm" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) . "?event_id=$updateeventID"; ?>">
        	<fieldset>
              <legend>New Event</legend>
              <p>
                <label for="event_name">Event name: </label>  
                <input type="text" name="event_name" id="event_name" value="<?php echo $event_name;?>" /> 
                <span class="errMsg"> <?php echo $event_nameErrMsg; ?></span>
              </p>
              <p>
                <label for="event_description">Event Description:</label>
                 <input type="text" name="event_description" id="event_description" value="<?php echo $eventDescription;?>" /> 
                <span class="errMsg"><?php echo $eventDescriptionErrMsg; ?></span>                
              </p>
              <p>
                <label for="event_presenter">Presenter: </label>
                <input type="text" name="event_presenter" id="event_presenter" value="<?php echo $eventPresenter;  ?>" /> 
                <span class="errMsg"><?php echo $eventPresenterErrMsg; ?></span>                      
              </p>
              <p>
                <label for="event_date">Date: </label> 
                <input type="text" name="event_date" id="event_date" value="<?php echo $eventDate;  ?>"/>
                <span class="errMsg"><?php echo $eventDateErrMsg; ?></span>      
              </p>
              <p>
                <label for="event_time">Event Time: </label> 
                <input type="text" name="event_time" id="event_time" value="<?php echo $eventTime;  ?>"/>
                <span class="errMsg"><?php echo $eventTimeErrMsg; ?></span>                
              </p>
              
             
             
                       
              
          </fieldset>
         	<p>
            	<input type="submit" name="submit" id="submit" value="Update Event" />
            	<input type="reset" name="button2" id="button2" value="Clear Form" onClick="clearForm()" />
        	</p>  
      </form>
        <?php
			}//end else
        ?>    	
	</main>
    
	

</div>
</body>
</html>

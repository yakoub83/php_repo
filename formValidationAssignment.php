
<?php
		//Setup the variables used by the page
		//field data
		$presenter_name = $presenter_ssn  =$presenter_radio=$presenter_honeypot="";
        
		//error messages
		$nameErrMsg = $ssnErrMsg  = $radioErrMsg=$honeypotErrMsg="";
       
		
      $validForm = false;

if(isset($_POST["submit"]))
	{	
		//The form has been submitted and needs to be processed
		
		//Validate the form data here!

		$presenter_name = $_POST['presenter_name'];
		$presenter_ssn = $_POST['presenter_ssn'];
        $presenter_radio = isset($_POST['RadioGroup1']);
		$presenter_honeypot = $_POST['honeypot'];
		
		//VALIDATION FUNCTIONS		Use functions to contain the code for the field validations.  
			function validateName($inName)
			{
				global $validForm, $nameErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
				$nameErrMsg = "";
				
				if($inName == "")
				{
					$validForm = false;
					$nameErrMsg = "Name cannot be spaces";
				}
				
				else
				{
					//Matches 111-22-3333 
					 if(!preg_match('/^[a-zA-Z ]*$/', $inName)) 
					 {
						$validForm = false;
						$nameErrMsg = "Invalid Name "; 
					 }
				}
			}//end validateName()
		
		
		function validateSsn($inSsn)
			{
				global $validForm, $ssnErrMsg;
				$ssnErrMsg = "";
				
				if(empty($inSsn))
				{
					$validForm = false;
					$ssnErrMsg = "Social Security required"; 					
				}
				else
				{
					//Matches 111-22-3333 
					 if(!preg_match('/^([0-9]){3}(([ ]|[-])?([0-9]){2})(([ ]|[-])?([0-9]){4})?$/', $inSsn)) 
					 {
						$validForm = false;
						$ssnErrMsg = "Invalid Social Security Number (111-22-3333 )"; 
					 }
				}
			}//end validateSsn()
		
		
		function validateRadio($inradio)
			{
				global $validForm, $radioErrMsg;
				$radioErrMsg = "";
				

				 if(!$inradio) {

          $validForm = false;

			 $radioErrMsg = "Please Select a Response Method";

        }

			}//end validateRadio()
		
		function honeypot($inval)
			{
				global $validForm, $honeypotErrMsg;
				$honeypotErrMsg = "";
				

				 if($inval!= '') {

             $validForm = false;

			 $honeypotErrMsg = "Spam detected!";

        }

			}//end honeypot()
		
		
		
		
		
		
		
		
			
		$validForm = true;		//switch for keeping track of any form validation errors
		
		validateName($presenter_name);
		validateSsn($presenter_ssn);
		validateRadio($presenter_radio);
		honeypot($presenter_honeypot);
		
		
		if($validForm)
		{
			$message = "All good";	
		}
		else
		{
			$message = "Something went wrong";
		}

}// ends if submit 
	

?>

<!DOCTYPE html>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - Form Validation Example</title>
<style>

#orderArea	{
	width:600px;
	background-color:#CF9;
}

.error	{
	color:red;
	font-style:italic;	
}
</style>
</head>

<body>
<h1>WDV341 Intro PHP</h1>
<h2>Form Validation Assignment</h2>
<?php
            //If the form was submitted and valid and properly put into database display the INSERT result message
			if($validForm)
			{
        ?>
            <h1><?php echo $message ?></h1>
        
        <?php
			}
			else	//display form
			{
        ?>


<div id="orderArea">
  <form id="form1" name="form1" method="post" action="formValidationAssignment.php">
  <h3>Customer Registration Form</h3>
  <table width="587" border="0">
    <tr>
      <td width="117">Name:</td>
      <td width="246"><input type="text" name="presenter_name" id="presenter_name" size="40" value="<?php echo $presenter_name;  ?>"/></td>
      <td width="210" class="error"><?php echo $nameErrMsg; ?></td>
    </tr>
    <tr>
      <td>Social Security</td>
      <td><input type="text" name="presenter_ssn" id="presenter_ssn" size="40" value="<?php echo $presenter_ssn; ?>" /></td>
      <td class="error"><?php echo $ssnErrMsg; ?></td>
    </tr>
    
    <!-- honeypot field start-->
    
     <tr style="display:none">
      <td>Keep this field blank</td>
      <td><input type="text" name="honeypot" id="honeypot" size="40" value="<?php echo $presenter_honeypot; ?>" /></td>
      <td class="error"><?php echo $honeypotErrMsg; ?></td>
    </tr>
    
    <!-- honeypot field end -->
    
    
    
    <tr>
      <td>Choose a Response</td>
      <td><p>
        <label>
          <input type="radio" name="RadioGroup1" id="RadioGroup1_0" value="phone" <?php if(isset($_POST['RadioGroup1']) && $_POST['RadioGroup1']=='phone') { echo ' checked="checked"'; } ?>>
          Phone</label>
        <br>
        <label>
          <input type="radio" name="RadioGroup1"   id="RadioGroup1_1" value="email" <?php if(isset($_POST['RadioGroup1']) && $_POST['RadioGroup1']=='email') { echo ' checked="checked"'; } ?> >
          Email</label>
        <br>
        <label>
          <input type="radio" name="RadioGroup1" id="RadioGroup1_2" value="usmail" <?php if(isset($_POST['RadioGroup1']) && $_POST['RadioGroup1']=='usmail') { echo ' checked="checked"'; } ?>>
          US Mail</label>
        <br>
      </p></td>     
      <td class="error"><?php echo $radioErrMsg; ?></td>
    </tr>
  </table>
  <p>
    <input type="submit" name="submit" id="button" value="Register" />
    <input type="reset" name="button2" id="button2" value="Clear Form" />
  </p>
</form>
 <?php
			
			}//end else
        ?>   
</div>

</body>
</html>
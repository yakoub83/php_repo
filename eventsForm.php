 <?php
$servername = "localhost";
$username = "root";
$password = "root";

try {
    $conn = new PDO("mysql:host=$servername;dbname=WDV341", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully";
	
	// prepare sql and bind parameters
    $stmt = $conn->prepare("INSERT INTO wdv341_event (event_name, event_description, event_presenter, event_date, event_time)
    VALUES (:eventname, :eventdescription, :eventpresenter, :eventdate, :eventtime )");
    $stmt->bindParam(':eventname', $eventName);
    $stmt->bindParam(':eventdescription', $eventDescription);
    $stmt->bindParam(':eventpresenter', $eventPresenter);
	$stmt->bindParam(':eventdate', $eventDate);
	$stmt->bindParam(':eventtime', $eventTime);

	$eventName ="newconnexion";
	$eventDescription="newconnexion";
	$eventPresenter="php";
	$eventDate="2018-08-05";
	$eventTime="18:00";
	
	
	$stmt->execute();
	
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }
?> 
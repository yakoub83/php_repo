<?php
	require 'adminPassCon.php';
	if(empty($_SESSION['validUser']))
	header('Location: adminLogin.php');
?>

<!doctype html>
<html>
<head>




<title>Contact V10</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="addForm/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/css/util.css">
	<link rel="stylesheet" type="text/css" href="addForm/css/main.css">
<!--===============================================================================================-->

	<meta charset="utf-8">
	<title>Presenting Information Technology</title>
	<link rel="stylesheet" href="css/pit.css">  
  	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">  	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  
  	
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
 	  
 	    	<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>  	
  	  
    <script>
		function clearForm() {
			//alert("inside clearForm()");
			$('.errMsg').html("");					//Clear all span elements that have a class of 'errMsg'. 		
			$('input:text').removeAttr('value');	//REMOVE the value attribute supplied by PHP Validations
			$('textarea').html("");					//Clear the textarea innerHTML
		}
	</script>

<style>
	
	h1{
		
		color: limegreen;
	}
body {margin: 0;}

ul.topnav {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

ul.topnav li {float: right;}

ul.topnav li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

ul.topnav li a:hover:not(.active) {background-color: #111;}

ul.topnav li a.active {background-color: #333;}

ul.topnav li.right {float: right;}

@media screen and (max-width: 600px){
    ul.topnav li.right, 
    ul.topnav li {float: none;}
}
</style>  
  
</head>

<body>

<div id="container">
     
<ul class="topnav">
  <li><a href="adminLogout.php">Logout</a></li>
  <li><a href="adminInsert.php">Add Info</a></li>
  <li class="right"><a href="adminDisplay.php">View Table</a></li>
</ul>


				<div class="container-contact100-form-btn">
				
				 <span><h1><?php  echo $_SESSION['message']; ?></h1></span>

				</div>
				
	
	
	</main>
  
</div>



<!--===============================================================================================-->
	<script src="addForm/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="addForm/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="addForm/vendor/bootstrap/js/popper.js"></script>
	<script src="addForm/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="addForm/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="addForm/vendor/daterangepicker/moment.min.js"></script>
	<script src="addForm/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="addForm/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="addForm/js/main.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>


</body>
</html>

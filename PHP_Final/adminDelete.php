<?php
//including the database connection file
require 'adminPassCon.php';
	
//getting id of the data from url
$id = $_GET['ID'];
 
//deleting the row from table   
$sql = "DELETE FROM Tourist WHERE ID=:id";
$stmt = $conn->prepare($sql);
$stmt->execute(array(':id' => $id));
 
$_SESSION['message']="Record has been successfully deleted";
header("Location: adminPage.php");
exit;
?>



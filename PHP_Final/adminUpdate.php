<?php
ob_start();
require 'adminPassCon.php';
	if(empty($_SESSION['validUser']))
		header('Location: adminLogin.php');

require 'adminConn.php';	//CONNECT to the database

$id = $_GET['ID'];
 
//selecting data associated with this particular id
$sql = "SELECT * FROM Tourist WHERE ID=:ID";
$stmt = $conn->prepare($sql);
$stmt->execute(array(':ID' => $id));
 

 $row=$stmt->fetch(PDO::FETCH_ASSOC);	 
			
	$trip_id=$row['ID'];
	$trip_name=$row['Name'];
	$trip_email=$row['Email'];
    $trip_destination=$row['Destination'];
	$trip_startdt=$row['Start_Date'];
	$trip_enddt=$row['End_Date'];   

	//error messages

	$trip_nameErrMsg="";
	$trip_emailErrMsg="";
	$trip_destinationErrMsg="";
	$trip_startdtErrMsg="";
    $trip_enddtErrMsg="";
	


		$validForm = false;

		
				
	if(isset($_POST["submit"]))
	{	
	
    $trip_name=$_POST['tourist_name'];
	$trip_email=$_POST['tourist_email'];
	$trip_destination=$_POST['tourist_destination'];
    $trip_startdt=$_POST['tourist_startdt'];   
    $trip_enddt=$_POST['tourist_enddt'];   
	
		//VALIDATION FUNCTIONS		Use functions to contain the code for the field validations.  
			function validateName($inVal){
		
		global $validForm, $trip_nameErrMsg;
			$trip_nameErrMsg = "";
			
        if($inVal=="") {
			$validForm = false;
            $trip_nameErrMsg= "this field can not be empty";
        }
		
	}// end validateName()
        
	function validateEmail($inVal){
		
		global $validForm, $trip_emailErrMsg;
			$trip_emailErrMsg = "";
		
        if($inVal=="") 
		{
			 $validForm = false;
            $trip_emailErrMsg="this field can not be empty";
        }
		
	}//end  validateEmail()
	
	 function validateDestination ($inVal){
		 
		 global $validForm, $trip_destinationErrMsg;
			$trip_destinationErrMsg = "";
        
        if($inVal=="") {
			$validForm = false;
            $trip_destinationErrMsg= "this field can not be empty";
        } 
	 }// end validateDestination()
	
		
	function validateStartDate($inVal){
		
		 global $validForm, $trip_startdtErrMsg;
			$trip_startdtErrMsg = "";
		
		if($inVal=="") {
			$validForm = false;
            $trip_startdtErrMsg= "this field can not be empty";
        }  
		
	}//end   validateStartDate()
		
		
		function validateEndDate($inVal){
		
		 global $validForm, $trip_enddtErrMsg;
			$trip_enddtErrMsg = "";
		
		if($inVal=="") {
			$validForm = false;
            $trip_enddtErrMsg= "this field can not be empty";
        }  
		
	}//end   validateEndDate()
			
		
		//VALIDATE FORM DATA  using functions defined above
		$validForm = true;		//switch for keeping track of any form validation errors
		
		validateName($trip_name);
		validateEmail($trip_email);
		validateDestination($trip_destination);
		validateStartDate($trip_startdt);
		validateEndDate($trip_enddt);
		
		if($validForm)
		{
			$_SESSION['message']="Record has been successfully Updated";
		}
		else
		{
			$_SESSION['message']="Something went wrong,Record not  Updated"; 
		}
			
}///end validation

?>



<!doctype html>
<html>
<head>

<title>Contact V10</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="addForm/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="addForm/css/util.css">
	<link rel="stylesheet" type="text/css" href="addForm/css/main.css">
<!--===============================================================================================-->




	<meta charset="utf-8">
	<title>Presenting Information Technology</title>
	<link rel="stylesheet" href="css/pit.css">  
  	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">  	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>  
  	
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
 	  
 	    	<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>  
 	
	<script>
		
	
	</script>   

    <script>
		function clearForm() {
			//alert("inside clearForm()");
			$('.errMsg').html("");					//Clear all span elements that have a class of 'errMsg'. 		
			$('input:text').removeAttr('value');	//REMOVE the value attribute supplied by PHP Validations
			$('textarea').html("");					//Clear the textarea innerHTML
		}
	</script>
	
	<style>
body {margin: 0;}

ul.topnav {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
}

ul.topnav li {float: right;}

ul.topnav li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

ul.topnav li a:hover:not(.active) {background-color: #111;}

ul.topnav li a.active {background-color: #333;}

ul.topnav li.right {float: right;}

@media screen and (max-width: 600px){
    ul.topnav li.right, 
    ul.topnav li {float: none;}
}
		
		.errMsg{
		font-style: italic;
		color: #d42a58;
		font-weight: bold;
	}
</style>  
  
  
  
</head>

<body>


		<?php
            //If the form was submitted and valid and properly put into database display the INSERT result message
			if($validForm)
			{
			
        ?>
        <?php
			 $sql = "UPDATE Tourist SET  Name=:name, Email=:email, Destination=:destination,Start_Date=:start_sate,End_Date=:end_date
		WHERE ID=:ID";
        $stmt = $conn->prepare($sql);
                
        $stmt->bindparam(':ID', $id);
        $stmt->bindparam(':name', $trip_name);
        $stmt->bindparam(':email', $trip_email);
		$stmt->bindparam(':destination', $trip_destination);
        $stmt->bindparam(':start_sate', $trip_startdt);
		$stmt->bindparam(':end_date', $trip_enddt);
        $stmt->execute();	
				
			header("Location: adminPage.php");
			exit;
	      ?>
<?php
			}
			else	//display form  errMesg
			{
?>
        



<div id="container">

	
    <main>
    
<ul class="topnav">
  <li><a href="adminLogout.php">Logout</a></li>
  <li><a href="adminInsert.php">Add a Record</a></li>
  <li class="right"><a href="adminDisplay.php">View Table</a></li>
</ul>

 
      
      <div class="container-contact100">

		<div class="wrap-contact100">
			<form class="contact100-form "  method="post" action="">
				<span class="contact100-form-title">
					Update 
				</span>

				<div class="wrap-input100 validate-input" >
					<input class="input100" type="text" name="tourist_name" value="<?php echo $trip_name;?>" placeholder="Full Name">
					<span class="focus-input100 "></span>
				</div>
				<span class="errMsg"> <?php echo $trip_nameErrMsg; ?></span>
				
				
				<div class="wrap-input100 validate-input" >
					<input class="input100" type="text" id="tourist_email" name="tourist_email" value="<?php echo $trip_email;?>" placeholder="Email">
					<span class="focus-input100 "></span>
				</div>
				<span class="errMsg"> <?php echo $trip_emailErrMsg; ?></span>
				
				
				<div class="wrap-input100 validate-input" >
					<input class="input100" type="text" id="tourist_destination" name="tourist_destination" value="<?php echo $trip_destination;?>" placeholder="Destination">
					<span class="focus-input100 "></span>
				</div>
				<span class="errMsg"> <?php echo $trip_destinationErrMsg; ?></span>

			<div class="wrap-input100 validate-input" >
					<input class="input100" type="text" id="tourist_startdt" name="tourist_startdt" value="<?php echo $trip_startdt;?>" placeholder="Start Date">
					<span class="focus-input100 "></span>
				</div>
				<span class="errMsg"> <?php echo $trip_startdtErrMsg; ?></span>

        	
			<div class="wrap-input100 validate-input" >
					<input class="input100" type="text" id="tourist_enddt" name="tourist_enddt" value="<?php echo $trip_enddt;?>" placeholder="End Date">
					<span class="focus-input100 "></span>
				</div>
				<span class="errMsg"> <?php echo $trip_enddtErrMsg; ?></span>
				

			
				<div class="container-contact100-form-btn">
				
				<button class="contact100-form-btn" type="submit" name="submit" id="submit" >
						<span>
							update
						</span>
					</button>
				
					<button class="contact100-form-btn" type="reset" name="button2" id="button2" value="Clear Form" onClick="clearForm()">
						<span>
							Reset
						</span>
					</button>
				</div>
				
			</form>
		</div>
	</div>


        <?php
			}//end else
        ?>    	
	</main>
  
</div>


<!--===============================================================================================-->
	<script src="addForm/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="addForm/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="addForm/vendor/bootstrap/js/popper.js"></script>
	<script src="addForm/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="addForm/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="addForm/vendor/daterangepicker/moment.min.js"></script>
	<script src="addForm/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="addForm/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="addForm/js/main.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>




	</body>
</html>

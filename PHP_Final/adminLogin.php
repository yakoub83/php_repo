<?php
	require 'adminPassCon.php';

	if(isset($_POST['login'])) {
		$errMsg = '';

		// Get data from FORM
		$username = $_POST['u'];    
		$password = $_POST['p'];
		
		
		if($username == '')
			$errMsg = 'Enter username';
		if($password == '')
			$errMsg = 'Enter password';

		if($errMsg == '') {
			try {
				$stmt = $conn->prepare('SELECT Tourist_user_id ,Tourist_user_name,Tourist_user_password  FROM Tourist_user WHERE Tourist_user_name = :username');
				$stmt->execute(array(
					':username' => $username
					));
				$data = $stmt->fetch(PDO::FETCH_ASSOC);

				if($data == false){
					$errMsg = "User $username not found.";
				}
				else {
					if($password == $data['Tourist_user_password']) {     ////
						$_SESSION['validUser'] =$data['Tourist_user_name'];
						$_SESSION['Tourist_user_password'] = $data['Tourist_user_password'];
						header('Location:adminDisplay.php');
						exit;
					}
					else
						$errMsg = 'Password not match.';
				}
			}
			catch(PDOException $e) {
				$errMsg = $e->getMessage();
			}
		}
	}
?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
<link href="../css/logincss.css" rel="stylesheet" type="text/css"  /><!-- css -->
</head>

<body>
<div class="login">

       	<?php
				if(isset($errMsg)){
					echo '<div style="color:#FF0000;text-align:center;font-size:17px;">'.$errMsg.'</div>';
				}
			?>
                     


	<h1>Login</h1>
    <form  action="php/adminLogin.php" method="post">
    	<input type="text" name="u" placeholder="Username" required="required" value="<?php if(isset($_POST['u'])) echo $_POST['u'] ?>"/>
        <input type="password" name="p" placeholder="Password" required="required" value="<?php if(isset($_POST['p'])) echo $_POST['p'] ?>"/>
        <button type="submit" class="btn btn-primary btn-block btn-large">Let me in.</button>
    </form>
</div>
</body>
</html>
